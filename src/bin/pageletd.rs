use pageletd::*;

use std::{path::Path, time::Duration};
use std::sync::mpsc::channel;

use notify::{Watcher, RecursiveMode};

fn main() -> std::io::Result<()> {
    let tera = tera().unwrap();
    let path = Path::new("/etc/pageletd/config.yaml");
    
    let config = reload_config(&path).unwrap();
    write_config(&tera, &config).unwrap();
    let handle = NginxHandle::new().unwrap();

    let (tx, rx) = channel();
    let mut watcher = notify::watcher(tx, Duration::from_secs(5)).unwrap();
    watcher.watch(&path, RecursiveMode::NonRecursive).unwrap();

    loop {
        match rx.recv() {
            Ok(_) => {
                let config = reload_config(&path).unwrap();
                write_config(&tera, &config).unwrap();
                handle.reload_config();
            },
            Err(_) => {
                eprintln!("Failed to watch for config changes");
            }
        }
    }
}
