FROM rust
RUN pwd
COPY Cargo.toml .
COPY src ./src
COPY templates ./templates
RUN cargo build --release

FROM nginx:latest
COPY --from=0 /target/release/pageletd /opt/bin/pageletd
RUN usermod -u 998 www-data
ENTRYPOINT ["/opt/bin/pageletd"]
