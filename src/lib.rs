use std::{fs::File, path::{Path, PathBuf}, process::Child};

use tera::Tera;
use serde::{Serialize, Deserialize};

#[derive(Debug)]
pub enum Error {
    BadTemplate,
    NoConfig,
    BadConfig,
    NoNginx,
    BadConfigWrite,
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Config {
    server_port: u32,
    pages_base: PathBuf,
    repos: Vec<Repo>
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Repo {
    url: String,
    mounts: Vec<Mount>,
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Mount {
    #[serde(default = "default_mount_path")]
    path: String,
    qualified_name: String
}

fn default_mount_path() -> String {
    "/".to_string()
}

pub fn tera() -> Result<Tera, Error> {
    let mut tera = Tera::default();
    tera.add_raw_template(
        "nginx.conf",
        include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/nginx.conf"))
    ).map_err(|e| Error::BadTemplate)?;
    Ok(tera)
}

pub fn reload_config(path: &Path) -> Result<Config, Error> {
    eprintln!("Reloading config from {:?}", path);
    let file = File::open(path)
        .map_err(|_| Error::NoConfig)?;
    let cfg = serde_yaml::from_reader(file)
        .map_err(|_| Error::BadConfig)?;
    Ok(cfg)
}

pub fn write_config(t: &Tera, cfg: &Config) -> Result<(), Error> {
    use tera::Context;
    
    let ctx = Context::from_serialize(&cfg)
        .map_err(|_| Error::BadConfig)?;
    let config = t.render("nginx.conf", &ctx)
        .map_err(|_| Error::BadTemplate)?;
    std::fs::write("/etc/nginx/nginx.conf", config)
        .unwrap();
    Ok(())
}

pub struct NginxHandle(Child);

impl NginxHandle {
    pub fn new() -> Result<Self, Error> {
        let child = std::process::Command::new("nginx")
            .spawn()
            .map_err(|_| Error::NoNginx)?;
        Ok(Self(child))
    }

    pub fn reload_config(&self) {
        use nix::unistd::Pid;
        use nix::sys::signal::SIGHUP;

        if let Err(_) = nix::sys::signal::kill(Pid::from_raw(self.0.id() as i32), SIGHUP) {
            eprintln!("Failed to send reload signal");
        }
    }
}
